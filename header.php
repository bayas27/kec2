<style>
  .blue-bg{
    background-color:#193358;
  }
  .navbar {
     border: none;
   }
   .mt-04{
    margin-top: 0.4%;
   }
   .yellow-bg{
      background-color:#dd9b25;
   }
   .font-white{
      color: #fff;
   }
   .f-s-14{
    font-size: 14px;
   }
   .f-s-12{
    font-size: 12px;
   }
   .tnea{
    margin:3% 2% 3% 2%;
    text-align:center;
   }
   .ml-1{
    margin-left:1%
   }
   .headertab{
      color: white;
   }
   .header-login{
    padding-top: 3%;
    font-size:12px;
    margin-left:20%;
    color: #fff;
    text-decoration: none;
   }
   .menubar{
    color:#2d2d2d;
    padding: 2%;
    float: right;
   }
</style>
<nav class="navbar navbar-custom" role="navigation">
  <div class="row headertab blue-bg ">
    <div class=" col-lg-offset-1  col-lg-6 col-md-offset-1  col-md-5 col-sm-offset-1  col-sm-5 mt-04">
      <img src="assets/images/kec_tw.png">
      <img src="assets/images/kec_fb.png">
      <img src="assets/images/kec_g.png">
      <img src="assets/images/kec_insta.png">
      <img src="assets/images/kec_you.png">
    </div>
    <div class="col-md-4 col-lg-3 col-sm-4 yellow-bg font-white">
      <p class="tnea"> <b>TNEA COUNSELLING CODE</b>
      <b>1207</b>
      </p>
    </div>
    <div class="col-md-2 col-lg-2 col-sm-2">
      <p class="header-login">
        <img src="assets/images/kec_login.png">
        <span>
          <b><a class="font-white" href="http://vinbytes.com:8000/#!/login">LOGIN</a></b>
        </span>
      </p>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row">
      <div  class=" col-lg-5 col-md-3 col-sm-3">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
              <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand page-scroll1" href="index.php">
              <img src="assets/images/kecLogo.png" class="img-responsive" width="50%"  >          
            </a>
          </div>    
        </div>
        <div class="col-lg-7 col-md-9 col-sm-9 menubar navbar-middle-custom">
          <div class="collapse navbar-collapse navbar-main-collapse" >
      <ul class="nav navbar-nav" >
        <li >
            <a class="page-scroll" href="" ><b>
              <div class="dropdown" >
                <span class="f-s-14"  ">Academicsaaaaaaaaa</span>
                
            </div>  
          </b>
        </a>
      </li>
        <li>
          <a class="page-scroll adm" href="admissions.php"><b>Admissions</b></a>
        </li>
        <li>
          <a class="page-scroll sch" href="scholarships.php"><b>Scholarships</b></a>
        </li>
        <li>
          <a class="page-scroll ab" href="about.php"><b>About</b></a>
        </li>
        <li>
          <a class="page-scroll con" href="contact.php"><b>Contact</b></a>
        </li>
        
        <a href="online.php" style="margin-left:15px"> <button style="border:none;border-radius:20px;padding:10px 18px 10px 18px;background-color:#00a0c6;color:white;font-size:13px;margin-top:0.7%;font-family:hkreg"><b>Apply Online</b></button></a>
        
      </ul>
    </div>    
  </div>
</div>
  </div>
</nav>